#tag Module
Protected Module Service
	#tag Method, Flags = &h1
		Protected Sub CenterWindow(CurrentWindow As Window)
		  CurrentWindow.left = Screen(0).width/2 - CurrentWindow.width/2
		  CurrentWindow.top = Screen(0).height/2 - CurrentWindow.height/2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Logout()
		  xojo.Core.Timer.CancelCall(AddressOf Utils.Service.Logout)
		  Do Until WindowCount = 0
		    Window(0).Close()   // Window(0) is the frontmost window
		  Loop
		  App.Session = new Model.SessionData
		  LoginWindow.Show
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
