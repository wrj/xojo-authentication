#tag Module
Protected Module Machine
	#tag Method, Flags = &h1
		Protected Function GetMachineName() As String
		  Dim current_user As String
		  Dim current_computer As String
		  #If TargetMacOS Then
		    
		    Try
		      Declare Function NSFullUserName Lib "Cocoa"  As CFStringRef
		      Declare Function SCDynamicStoreCopyComputerName Lib "SystemConfiguration.framework" (store As Ptr, nameEncoding As Ptr) As CFStringRef
		      current_user=NSFullUserName
		      current_computer = SCDynamicStoreCopyComputerName(Nil, Nil)
		    Catch
		    End Try
		    
		  #ElseIf TargetWin32
		    Declare Sub GetUserNameA Lib "AdvApi32" ( name As Ptr, ByRef size As Integer )
		    Declare Sub GetUserNameW Lib "AdvApi32" ( name As Ptr, ByRef size As Integer )
		    Declare Sub GetComputerNameA Lib "Kernel32" ( name As Ptr, ByRef size As Integer )
		    Declare Sub GetComputerNameW Lib "Kernel32" ( name As Ptr, ByRef size As Integer )
		    '
		    Dim mb As New MemoryBlock(1024 )
		    Dim size As Integer = mb.Size
		    
		    If System.IsFunctionAvailable( "GetUserNameW", "AdvApi32" ) Then
		      GetUserNameW( mb, size )
		      current_user=mb.WString( 0 )
		    Else
		      GetUserNameA( mb, size )
		      current_user=mb.CString( 0 )
		    End If
		    If System.IsFunctionAvailable( "GetComputerNameW", "Kernel32" ) Then
		      GetComputerNameW( mb, size )
		      current_computer=mb.WString( 0 )
		    Else
		      GetComputerNameA( mb, size )
		      current_computer=mb.CString( 0 )
		    End If
		  #EndIf
		  Return current_computer
		  'MsgBox "You are "+current_user+" using "+current_computer
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
