#tag WebPage
Begin WebDialog LoginDialog
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   128
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   False
   Style           =   "None"
   TabOrder        =   0
   Title           =   "Untitled"
   Top             =   0
   Type            =   3
   VerticalCenter  =   0
   Visible         =   True
   Width           =   294
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   129
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   295
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebButton btnLogin
      AutoDisable     =   False
      Caption         =   "Login"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   112
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   3
      Top             =   87
      VerticalCenter  =   0
      Visible         =   True
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebTextField fldPassword
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   112
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   "False"
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   53
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebTextField fldUsername
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   112
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   "False"
      ReadOnly        =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   19
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebLabel lblPassword
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Password :"
      TextAlign       =   3
      Top             =   53
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebLabel lblUsername
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Username :"
      TextAlign       =   3
      Top             =   19
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   "False"
      _HorizontalPercent=   "0.0"
      _IsEmbedded     =   "False"
      _Locked         =   "False"
      _NeedsRendering =   True
      _OfficialControl=   "False"
      _OpenEventFired =   "False"
      _VerticalPercent=   "0.0"
   End
   Begin WebButton btnCancel
      AutoDisable     =   False
      Caption         =   "Cancel"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   188
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "-1"
      TabOrder        =   4
      Top             =   87
      VerticalCenter  =   0
      Visible         =   True
      Width           =   74
      ZIndex          =   1
      _NeedsRendering =   True
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Dismissed()
		  If Session.LoggedIn = False Then
		    If Session.CurrentPage.Title <> "index" Then
		      IndexPage.Show
		    End If
		  Else
		    If Session.CurrentPage.Title = "index" Then
		      DashboardPage.Show
		    End If
		  End If
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub BindData()
		  Dim data As New JSONItem
		  
		  data.Value("username") = Self.fldUsername.Text
		  data.Value("password") = Self.fldPassword.Text
		  
		  Self.formData = data
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Login()
		  Dim ls As New Repo.WS.Login
		  If Utils.Network.ConnectServer Then
		    If ls.authentication(formData) Then
		      Self.Close
		      DashboardPage.Show
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ValidateForm() As Boolean
		  If Trim(Self.fldUsername.Text) <> "" And Trim(Self.fldPassword.Text) <> "" Then
		    Return True
		  Else
		    Dim errMessage As String
		    If Trim(Self.fldUsername.Text) = "" Then
		      errMessage = "Username must not Blank."+EndOfLine
		    End If
		    If Trim(Self.fldPassword.Text) = "" Then
		      errMessage = errMessage+"Password must not Blank."
		    End If
		    MsgBox errMessage
		    Return False
		  End If
		End Function
	#tag EndMethod


	#tag Note, Name = Username
		Username : admin
		Password : password
		
	#tag EndNote


	#tag Property, Flags = &h1
		Protected formData As JSONItem
	#tag EndProperty


#tag EndWindowCode

#tag Events btnLogin
	#tag Event
		Sub Action()
		  If Self.ValidateForm Then
		    Self.BindData
		    Self.Login
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldPassword
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case details.KeyCode
		  Case details.KeyEnter
		    If Self.ValidateForm Then
		      Self.BindData
		      Self.Login
		    End If
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  Self.Close
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="3"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
