#tag Class
Protected Class Login
	#tag Method, Flags = &h0
		Function authentication(formData As JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim rawData As JSONItem
		  Dim data() As String
		  
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json")
		  data.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  c.SetOptionHTTPHeader(data)
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + "" + App.kAuthentication
		  If c.Perform = 0 Then
		    If c.GetInfoResponseCode <> 401 Then
		      // 401 Spring Security Rest return when username and password failed.
		      rawData = New JSONItem(c.OutputData)
		      Dim sd As New Model.SessionData
		      sd.Username = rawData.Value("username")
		      sd.Token = rawData.Value("access_token")
		      getUserDetail(sd)
		      Return True
		    Else
		      MsgBox "Username or Password is wrong!"
		      Return False
		    End If
		  Else
		    MsgBox "Network Error."
		    Return False
		  End If
		  // End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub getUserDetail(sd As Model.SessionData)
		  Dim c As New CURLSMBS
		  Dim rawData As JSONItem
		  Dim data() As String
		  
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json")
		  data.Append("Authorization:"+sd.Token)
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  c.SetOptionHTTPHeader(data)
		  c.OptionURL = App.kURLService + "" + App.kProfile
		  If c.Perform = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    sd.Firstname = rawData.Value("firstname")
		    sd.Lastname = rawData.Value("lastname")
		    Session.SessionData = sd
		    Session.LoggedIn = True
		    MsgBox "Welcome "+sd.Firstname+" "+sd.Lastname
		  End If
		  // End
		End Sub
	#tag EndMethod


	#tag Note, Name = MBS Response
		
		Method GetInfoResponseCode
		
		200 = status Ok
		401 = Username หรือ Password เวลา login ไม่ผ่าน
		403 = Forbidden มี json return {"timestamp":1466756342434,"status":403,"error":"Forbidden","message":"Access Denied","path":"/api/logind"}
		
		
		
		
		Method Perform
		
		CURLE_OK                     0 No error.
		CURLE_UNSUPPORTED_PROTOCOL   1 Unsupported protocol.
		CURLE_FAILED_INIT            2 Initialization failed or option not available.
		CURLE_URL_MALFORMAT          3 URL has wrong format.
		
		https://www.mbsplugins.eu/CURLPerform.shtml
	#tag EndNote


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
