#tag Class
Protected Class App
Inherits WebApplication
	#tag Constant, Name = kAuthentication, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"/api/login"
	#tag EndConstant

	#tag Constant, Name = kCheckServer, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"/server"
	#tag EndConstant

	#tag Constant, Name = kProfile, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"/profile"
	#tag EndConstant

	#tag Constant, Name = kSessionLife, Type = Double, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"0.3"
	#tag EndConstant

	#tag Constant, Name = kURLService, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"http://www.brainforsell.com:8080"
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
