#tag Module
Protected Module Service
	#tag Method, Flags = &h1
		Protected Sub CheckLogin()
		  If Session.LoggedIn = False Then
		    MsgBox "Please login."
		    Session.CurrentPage.Close
		    Dim loginDia As New LoginDialog
		    loginDia.Show
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Logout()
		  Session.CurrentPage.Close
		  Session.SessionData = New Model.SessionData
		  Session.LoggedIn = False
		  MsgBox "Logout complete."
		  IndexPage.Show
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
