#tag Module
Protected Module Network
	#tag Method, Flags = &h1
		Protected Function ConnectServer() As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + "" + App.kCheckServer
		  c.OptionHeader = False
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  If c.Perform = 0 Then
		    Return True
		  Else
		    MsgBox "Can't connect to server."
		    Return False
		  End
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
